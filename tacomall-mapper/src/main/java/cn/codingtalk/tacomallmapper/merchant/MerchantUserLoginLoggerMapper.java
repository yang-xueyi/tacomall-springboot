/***
 * @Author: 码上talk|RC
 * @Date: 2020-09-27 15:26:27
 * @LastEditTime: 2020-09-27 15:26:43
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-springboot/tacomall-mapper/src/main/java/cn/codingtalk/tacomallmapper/merchant/MerchantUserLoginLoggerMapper.java
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
package cn.codingtalk.tacomallmapper.merchant;

import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.codingtalk.tacomallentity.merchant.MerchantUserLoginLogger;

@Repository
public interface MerchantUserLoginLoggerMapper extends BaseMapper<MerchantUserLoginLogger> {

}